//
//  CityWeather.swift
//  Weather
//
//  Created by Данил Черников on 16.01.2019.
//  Copyright © 2019 dchernikov. All rights reserved.
//

import Foundation
import UIKit

struct CityWeather : Codable {
    let name : String
    let weather : [Weather]
    let main : Main
}

struct Weather : Codable{
    let description: String
    let icon : String
}

struct Main : Codable {
    let temp: Double
    let pressure: Double
    let humidity: Int
}


