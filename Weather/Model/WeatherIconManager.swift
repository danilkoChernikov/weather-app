//
//  IconManager.swift
//  Weather
//
//  Created by Данил Черников on 16.01.2019.
//  Copyright © 2019 dchernikov. All rights reserved.
//

import Foundation
import UIKit

enum WeatherIconManager: String {
    
    case ClearSky = "clear sky"
    case FewClouds = "few clouds"
    case ScatteredClouds = "scattered clouds"
    case BrokenClouds = "broken clouds"
    case ShowerRain = "shower rain"
    case Rain = "rain"
    case Thunderstorm = "thunderstorm"
    case Snow = "snow"
    case Mist = "mist"
    
    init(rawValue: String){
        switch rawValue {
        case "01d", "01n": self = .ClearSky
        case "02d", "02n": self = .FewClouds
        case "03d", "03n": self = .ScatteredClouds
        case "04d", "04n": self = .BrokenClouds
        case "09d", "09n": self = .ShowerRain
        case "10d", "10n": self = .Rain
        case "11d", "11n": self = .Thunderstorm
        case "13d", "13n": self = .Snow
        case "50d", "50n": self = .Mist
        default: self = .ScatteredClouds
        }
    }
}

extension WeatherIconManager {
    var image: UIImage {
        return UIImage(named: self.rawValue)!
    }
}
