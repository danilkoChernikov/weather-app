//
//  City.swift
//  Weather
//
//  Created by Данил Черников on 22.01.2019.
//  Copyright © 2019 dchernikov. All rights reserved.
//

import Foundation

struct City: Decodable{
    let name: String
}
