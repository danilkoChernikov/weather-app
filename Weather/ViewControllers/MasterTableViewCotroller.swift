//
//  MasterTableViewCotroller.swift
//  Weather
//
//  Created by Данил Черников on 16.01.2019.
//  Copyright © 2019 dchernikov. All rights reserved.
//

import UIKit
import CoreData

class MasterTableViewCotroller: UITableViewController, SearchTableViewControllerDelegate{
   
    func favoriteCity(favoriteUserCity: City) {
        self.userFavoriteCitiesList.append(favoriteUserCity)
        createCitiesListInCoreData(data: filteringOfDublicate(userFavoriteCitiesList))
    }
    
    var userFavoriteCitiesList = [City]()
    var searchResultsList = [City]()
    
    var searchFunctions : SearchFunctions<City>!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        retrieveData()
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        initializationSearchBar()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return searchFunctions.isFiltering() ? searchResultsList.count : userFavoriteCitiesList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath)
        
        let city = searchFunctions.isFiltering() ? searchResultsList[indexPath.row] : userFavoriteCitiesList[indexPath.row]

        cell.textLabel?.text = city.name

        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            deleteData(indexPath: indexPath.row)
            self.tableView.reloadData()
        }
    }
    
    ///MARK: - Creating records in CoreData
    func createCitiesListInCoreData(data: [City]){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{return}
        let context = appDelegate.persistentContainer.viewContext
        
        let cities = NSEntityDescription.entity(forEntityName: "Cities", in: context)!
        
        for element in data{
            let city = NSManagedObject(entity: cities, insertInto: context)
            city.setValue(element.name, forKey: "name")
        
        do{
            try context.save()
        }catch{
            print("\n Failed save \n \(error.localizedDescription)")
        }
        }
        
    }
    
    /// MARK: - Data recovery from CoreData
    func retrieveData(){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{return}
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Cities")
        
        do{
            let result = try context.fetch(fetchRequest)
            DispatchQueue.global(qos: .utility).async {
                for data in result {
                    let city = data.value(forKey: "name") as! String
                    self.userFavoriteCitiesList.append(City(name: city))
                }
                self.userFavoriteCitiesList = self.filteringOfDublicate(self.userFavoriteCitiesList)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }catch{
            print("Failde retrieve data - \(error.localizedDescription)")
        }
    }
    
    ///MARK: - Delete in CoreData
    func deleteData(indexPath: Int){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{return}
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Cities")
        
        do{
            let fetch = try context.fetch(fetchRequest)
            let objectToDelete = fetch[indexPath]
            context.delete(objectToDelete)
            userFavoriteCitiesList.remove(at: indexPath)
            
            do{
                try context.save()
            }catch{
                print(error)
            }
        }catch{
            print(error)
        }
        
    }
    
    
    // MARK: - Filtering Duplicate
    func filteringOfDublicate( _ arrayCitiesWithDuplicate: [City] ) -> [City]{
        var seen = Set<String>()
        var arrayCitiesWithoutDuplicates = [City]()
        for cities in arrayCitiesWithDuplicate{
            if !seen.contains(cities.name){
            arrayCitiesWithoutDuplicates.append(cities)
            seen.insert(cities.name)
            }
        }
        return arrayCitiesWithoutDuplicates
    }
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SearchCitys"{
            guard let searchTableViewController = segue.destination as? SearchTableViewController else{return}
            searchTableViewController.delegate = self
        }else if segue.identifier == "CityDetailWeather"{
            if let indexPath = tableView.indexPathForSelectedRow{
                guard let detailViewController = segue.destination as? DetailViewController else{return}
                let dataCity = searchFunctions.isFiltering() ? searchResultsList[indexPath.row] : userFavoriteCitiesList[indexPath.row]
                detailViewController.cityName = dataCity.name
            }
            
        }
    }
    
    func initializationSearchBar(){
        self.searchFunctions = SearchFunctions(cityWeatherList: self.userFavoriteCitiesList,
                                               searchResultList: self.searchResultsList, searchController: UISearchController(searchResultsController: nil))
        
        searchFunctions.searchController.searchResultsUpdater = self
        searchFunctions.searchController.obscuresBackgroundDuringPresentation = false
        searchFunctions.searchController.searchBar.placeholder = "Search City"
        navigationItem.searchController = searchFunctions.searchController
        navigationItem.hidesSearchBarWhenScrolling = false
    }
    
    // MARK: - Search
    
    func filterContentForSearchText(searchCity: String){
        self.searchResultsList = self.userFavoriteCitiesList.filter({ (city: City) -> Bool in
            return city.name.lowercased().contains(searchCity.lowercased())
        })
        self.tableView.reloadData()
    }

    

}

extension MasterTableViewCotroller: UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchCity: searchController.searchBar.text!)
    }
}
