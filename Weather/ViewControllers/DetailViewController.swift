//
//  DetailViewController.swift
//  Weather
//
//  Created by Данил Черников on 16.01.2019.
//  Copyright © 2019 dchernikov. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class DetailViewController: UIViewController {
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var weatherTypeLabel: UILabel!
    @IBOutlet weak var weatherPressureLabel: UILabel!
    @IBOutlet weak var weatherHumidityLabel: UILabel!
    @IBOutlet weak var weatherTemperatureCelsiusLabel: UILabel!
    @IBOutlet weak var weatherTemperatureFahrenheitLabel: UILabel!
    @IBOutlet weak var weatherMeasureSwitch: UISegmentedControl!
    
    let OpenWeatherApiKey = "31197dea5826b5facaf1350180d78672"
    
    var cityName : String = ""
    let noneWeatheData = CityWeather(name: "None", weather:[Weather(description: "none", icon: "none")], main: Main(temp: 0.0, pressure: 0.0, humidity: 0 ))

    override func viewDidLoad() {
        super.viewDidLoad()
        updateWeather(nameCity: self.cityName)
    }
    
    func updateWeather(nameCity: String){
       updateDetailScreen(currentWeather: recoveryCityWeatherFromCoreData(nameCity: nameCity))
        
        let pathString = "https://api.openweathermap.org/data/2.5/weather?q=\(cityName)&appid=\(OpenWeatherApiKey)"
        
        guard let url = URL(string: pathString) else {return}
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error == nil {
                guard let data = data else{return}
                
                do{
                    let json = try JSONDecoder().decode(CityWeather.self, from: data)
                    DispatchQueue.main.async {
                        self.readingRespondsJSON(response: json)
                    }
            }catch let err{
                print(err.localizedDescription)
                }
            }
        }.resume()
    }
    
    func readingRespondsJSON(response: CityWeather){
        
        let weather = CityWeather(name: response.name,
                                  weather: [Weather(description: response.weather[0].description, icon: response.weather[0].icon)],
                                  main: Main(temp: response.main.temp, pressure: response.main.pressure, humidity: response.main.humidity))
        
        saveWeatherFromRespondsInCoreData(data: weather)
    }
    
    func saveWeatherFromRespondsInCoreData(data: CityWeather){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{return}
        let context = appDelegate.persistentContainer.viewContext
        
        let entityCitiesWeather = NSEntityDescription.entity(forEntityName: "CitiesWeather", in: context)!
        
        let cityWeather = NSManagedObject(entity: entityCitiesWeather, insertInto: context)
        cityWeather.setValue(data.name, forKey: "name")
        cityWeather.setValue(data.main.temp, forKey: "temp")
        cityWeather.setValue(data.main.pressure, forKey: "pressure")
        cityWeather.setValue(data.main.humidity, forKey: "humidity")
        cityWeather.setValue(data.weather[0].description, forKey: "main")
        cityWeather.setValue(data.weather[0].icon, forKey: "icon")
        
        do{
           try context.save()
            self.updateDetailScreen(currentWeather: data)
        }catch{
            print(error.localizedDescription)
        }
        
    }
    
    func recoveryCityWeatherFromCoreData(nameCity: String) -> CityWeather{
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let context = appDelegate!.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CitiesWeather")
        
        var data : CityWeather
        
        
        do{
            let result = try context.fetch(fetchRequest)
            
            for element in result{
                if element.value(forKey: "name") as? String == nameCity{
                
                    let name = element.value(forKey: "name") as? String
                    let temperature = element.value(forKey: "temp") as? Double
                    let pressure = element.value(forKey: "pressure") as? Double
                    let humidity = element.value(forKey: "humidity") as? Int
                    let description = element.value(forKey: "main") as? String
                    let icon = element.value(forKey: "icon") as? String
                
                    data = CityWeather(name: name!,
                                       weather: [Weather(description: description!, icon: icon!)],
                                       main: Main(temp: temperature!, pressure: pressure!, humidity: humidity!))
                    return data
                }
            }
        }catch{
            print(error.localizedDescription)
        }
        return noneWeatheData
        
    }
    
    func updateDetailScreen(currentWeather: CityWeather){
        
        self.cityNameLabel?.text = currentWeather.name
        
        let image = WeatherIconManager(rawValue: currentWeather.weather[0].icon)
        self.imageView.image = image.image
        
        self.weatherTypeLabel?.text = currentWeather.weather[0].description
        self.weatherTemperatureCelsiusLabel?.text = currentWeather.getWeatherTemperatureCelcius
        self.weatherTemperatureFahrenheitLabel?.text = currentWeather.getWeatherTemperatureFahrenheit
        self.weatherPressureLabel?.text = currentWeather.getWeatherPressure
        self.weatherHumidityLabel?.text = currentWeather.getWeatherHumidity
        
    }
    
    @IBAction func tappedTheMeasureSwitch(_ sender: UISegmentedControl) {
        switch self.weatherMeasureSwitch.selectedSegmentIndex {
        case 0:
            isHiddenTemperatureLabel(hide: false)
        case 1:
            isHiddenTemperatureLabel(hide: true)
        default:
            break
        }
    }
    
    func isHiddenTemperatureLabel(hide: Bool){
        self.weatherTemperatureCelsiusLabel.isHidden = hide
        self.weatherTemperatureFahrenheitLabel.isHidden = !hide
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CityWeather{
    var getWeatherPressure : String{
        return "\(self.main.pressure)mm"
    }
    var getWeatherHumidity: String{
        return "\(self.main.humidity)%"
    }
    var getWeatherTemperatureCelcius: String{
        return "\(Int(self.main.temp - 273))˚C"
    }
    var getWeatherTemperatureFahrenheit: String{
        return "\(Int(1.8 * (self.main.temp - 273) + 32))˚F"
    }
}
