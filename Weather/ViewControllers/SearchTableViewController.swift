//
//  SearchTableViewController.swift
//  Weather
//
//  Created by Данил Черников on 16.01.2019.
//  Copyright © 2019 dchernikov. All rights reserved.
//

import UIKit

protocol SearchTableViewControllerDelegate {
    func favoriteCity(favoriteUserCity: City)
}

class SearchTableViewController: UITableViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    
    var citiesList = [City]()
    
    var favoriteUserCities = [City]()
    var citySearchResultList = [City]()
    
    var delegate : SearchTableViewControllerDelegate?
    
    var searchFunctions : SearchFunctions<City>!


    override func viewDidLoad() {
        super.viewDidLoad()
        if(citiesList.count == 0){
            readingCityListFromFileJSON()
        }
        initializationSearchBar()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return searchFunctions.isFiltering() ? citySearchResultList.count : favoriteUserCities.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchingCityCell", for: indexPath)
        let cityInformation = searchFunctions.isFiltering() ? citySearchResultList[indexPath.row] : favoriteUserCities[indexPath.row]
        cell.textLabel?.text = cityInformation.name
        // Configure the cell...
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchFunctions.isFiltering(){
            let favoriteCity = citySearchResultList[indexPath.row]
            favoriteUserCities.append(favoriteCity)
            delegate?.favoriteCity(favoriteUserCity: favoriteCity)
        }
    }
    
    // initialization Search Bar
    func initializationSearchBar(){
        self.searchFunctions = SearchFunctions(cityWeatherList: self.citiesList,
                                               searchResultList: self.citySearchResultList, searchController: UISearchController(searchResultsController: nil))
        
        searchFunctions.searchController.searchResultsUpdater = self
        searchFunctions.searchController.obscuresBackgroundDuringPresentation = false
        searchFunctions.searchController.searchBar.placeholder = "Search City"
        navigationItem.searchController = searchFunctions.searchController
        navigationItem.hidesSearchBarWhenScrolling = false

    }

    // reding file format json with list cities
    func readingCityListFromFileJSON(){
        
        let url = Bundle.main.url(forResource: "cityList", withExtension: "json")!
        
        let data = try! Data(contentsOf: url)
        let queue = DispatchQueue.global(qos: .utility)
        queue.async {
            do{
                let JSON = try JSONDecoder().decode([City].self, from: data)
                
                DispatchQueue.main.async {
                    self.citiesList = JSON
                }
            } catch let jsonError{
                print(jsonError)
            }
        }
    }
    
    func filterContentForSearchText(searchCity: String){
        self.citySearchResultList = self.citiesList.filter({ (city: City) -> Bool in
            return city.name.lowercased().contains(searchCity.lowercased())
        })
        self.tableView.reloadData()
    }
}

extension SearchTableViewController: UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchCity: searchController.searchBar.text!)
    }
}
