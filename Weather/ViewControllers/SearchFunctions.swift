//
//  Preference.swift
//  Weather
//
//  Created by Данил Черников on 22.01.2019.
//  Copyright © 2019 dchernikov. All rights reserved.
//

import Foundation
import UIKit

struct SearchFunctions<T> {
    
    var searchResultsList : [T]
    var cityWeatherList : [T]
    let searchController : UISearchController
    
    init(cityWeatherList:[T], searchResultList: [T], searchController: UISearchController) {
        self.cityWeatherList = cityWeatherList
        self.searchResultsList = searchResultList
        self.searchController = searchController
    }
    
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    func isFiltering() -> Bool{
        return !searchBarIsEmpty() && searchController.isActive
    }

}
